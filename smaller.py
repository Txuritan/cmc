import os
import subprocess
import time

storage_path = "store"

for folder in os.listdir(storage_path):
    full_folder = os.path.join(storage_path, folder)

    if os.path.isdir(full_folder):
        print("[" + folder + "]: Start")

        for file in os.listdir(full_folder):
            if not file.endswith("-small.png"):
                full_file = os.path.join(full_folder, file)

                subprocess.call([
                    "C:\Program Files\ImageMagick-7.0.8-Q16\convert.exe",
                    full_file,
                    "-resize", "145x200",
                    os.path.splitext(full_file)[0] + "-small.png"
                ])

                time.sleep(0.25)

        print("[" + folder + "]: End")