BEGIN;

CREATE TABLE IF NOT EXISTS artists(
    id              TEXT            PRIMARY KEY,
    name            TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS authors(
    id              TEXT            PRIMARY KEY,
    name            TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS categories(
    id              TEXT            PRIMARY KEY,
    name            TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS characters(
    id              TEXT            PRIMARY KEY,
    name            TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS comics(
    id              TEXT            PRIMARY KEY,
    title           TEXT            NOT NULL,
    pages           INTEGER         NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS groups(
    id              TEXT            PRIMARY KEY,
    name            TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS parodies(
    id              TEXT            PRIMARY KEY,
    title           TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS tags(
    id              TEXT            PRIMARY KEY,
    title           TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS series(
    id              TEXT            PRIMARY KEY,
    title           TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS thumbnails(
    comic_id        TEXT            NOT NULL,
    place           INTEGER         NOT NULL,
    data            BLOB            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS comic_artists(
    comic_id        TEXT            NOT NULL,
    artist_id       TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(artist_id) REFERENCES artists(id)
);

CREATE TABLE IF NOT EXISTS comic_authors(
    comic_id        TEXT            NOT NULL,
    author_id       TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(author_id) REFERENCES authors(id)
);

CREATE TABLE IF NOT EXISTS comic_category(
    comic_id        TEXT            NOT NULL,
    category_id    TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

CREATE TABLE IF NOT EXISTS comic_characters(
    comic_id        TEXT            NOT NULL,
    character_id    TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(character_id) REFERENCES characters(id)
);

CREATE TABLE IF NOT EXISTS comic_groups(
    comic_id        TEXT            NOT NULL,
    group_id        TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(group_id) REFERENCES groups(id)
);

CREATE TABLE IF NOT EXISTS comic_parodies(
    comic_id        TEXT            NOT NULL,
    parody_id       TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(parody_id) REFERENCES parodies(id)
);

CREATE TABLE IF NOT EXISTS comic_tags(
    comic_id        TEXT            NOT NULL,
    tag_id          TEXT            NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(tag_id) REFERENCES tags(id)
);

CREATE TABLE IF NOT EXISTS comic_series(
    comic_id        TEXT            NOT NULL,
    series_id       TEXT            NOT NULL,
    place           INTEGER         NOT NULL,

    created         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,
    updated         TEXT            NOT NULL
                                    DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY(comic_id) REFERENCES comics(id),
    FOREIGN KEY(series_id) REFERENCES series(id)
);

COMMIT;
