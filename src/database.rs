use {
    crate::{error::Error, models::*, pages::AddForm},
    chrono::prelude::*,
    rusqlite::{OptionalExtension, ToSql, NO_PARAMS},
    uuid::Uuid,
};

pub type Pool = r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>;

#[derive(Clone)]
pub struct Database {
    pool: Pool,
}

impl Database {
    pub fn new() -> Self {
        let pool = r2d2::Pool::new(r2d2_sqlite::SqliteConnectionManager::file("cmc3.db"))
            .expect("unable to create sqlite connection pool");

        pool.clone()
            .get()
            .expect("no open connections in the connection pool")
            .execute_batch(include_str!("../create.sql"))
            .expect("database schema could not be committed");

        Self { pool }
    }

    pub fn get_all_comics(&self) -> Result<Vec<Comic>, Error> {
        let conn = self.pool.get()?;
        let mut stmt = conn.prepare("SELECT id, title, pages FROM comics ORDER BY title;")?;
        let comic_rows = stmt.query_map(
            NO_PARAMS,
            |row| -> rusqlite::Result<(String, String, u32)> {
                Ok((row.get("id")?, row.get("title")?, row.get("pages")?))
            },
        )?;

        comic_rows
            .map(|comic_row| {
                comic_row
                    .map_err(Error::from)
                    .and_then(|row| Comic::from_row(self.pool.clone(), row))
            })
            .collect()
    }

    pub fn get_comic(&self, id: String) -> Result<Comic, Error> {
        let conn = self.pool.get()?;

        let row: (String, String, u32) = conn.query_row(
            "SELECT ID, title, pages FROM comics WHERE id = ?;",
            &[&id],
            |row| Ok((row.get("id")?, row.get("title")?, row.get("pages")?)),
        )?;

        let comic = Comic::from_row(self.pool.clone(), row)?;

        Ok(comic)
    }

    pub fn search_comics(&self, search: &str) -> Result<Vec<Comic>, Error> {
        let mut and = Vec::new();
        let mut not = Vec::new();

        let tags = search.split(',');

        for tag in tags {
            if tag.starts_with('-') {
                not.push(
                    tag.trim_start_matches('-')
                        .trim_start()
                        .trim_end()
                        .to_string(),
                );
            } else {
                and.push(tag.trim_start().trim_end().to_string());
            }
        }

        let mut query = String::from("SELECT comic.* FROM comic_tags comic_tag, comics comic, tags tag WHERE comic_tag.tag_id = tag.id AND comic.id = comic_tag.comic_id");

        if !and.is_empty() {
            query.push_str(" AND (lower(tag.title) IN (");

            for (i, _) in and.iter().enumerate() {
                query.push_str("lower(?)");

                if i != and.len() - 1 {
                    query.push_str(", ");
                }
            }

            query.push_str("))");
        }

        if !not.is_empty() {
            query.push_str(" AND comic.id NOT IN (SELECT comic.id FROM comics comic, comic_tags comic_tag, tags tag WHERE comic.id = comic_tag.comic_id AND comic_tag.tag_id = tag.id AND (lower(tag.title) IN (");

            for (i, _) in not.iter().enumerate() {
                query.push_str("lower(?)");

                if i != and.len() - 1 {
                    query.push_str(", ");
                }
            }

            query.push_str(")))");
        }

        query.push_str(" GROUP BY comic.title;");

        let conn = self.pool.get()?;
        let mut stmt = conn.prepare(&query)?;
        let comic_rows = stmt.query_map(
            and.iter()
                .chain(&not)
                .map(|t| t as &ToSql)
                .collect::<Vec<_>>(),
            |row| -> rusqlite::Result<(String, String, u32)> {
                Ok((row.get("id")?, row.get("title")?, row.get("pages")?))
            },
        )?;

        comic_rows
            .map(|comic_row| {
                comic_row
                    .map_err(Error::from)
                    .and_then(|row| Comic::from_row(self.pool.clone(), row))
            })
            .collect()
    }

    pub fn get_artist_comics(&self, artist: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_artists comic_artist ON comic.id = comic_artist.comic_id WHERE comic_artist.artist_id = ? ORDER BY comic.title;",
            &artist,
        )
    }

    pub fn get_category_comics(&self, category: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_category comic_category ON comic.id = comic_category.comic_id WHERE comic_category.category_id = ? ORDER BY comic.title;",
            &category,
        )
    }

    pub fn get_character_comics(&self, character: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_characters comic_character ON comic.id = comic_character.comic_id WHERE comic_character.character_id = ? ORDER BY comic.title;",
            &character,
        )
    }

    pub fn get_group_comics(&self, group: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_groups comic_group ON comic.id = comic_group.comic_id WHERE comic_group.group_id = ? ORDER BY comic.title;",
            &group,
        )
    }

    pub fn get_parody_comics(&self, parody: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_parodies comic_parody ON comic.id = comic_parody.comic_id WHERE comic_parody.parody_id = ? ORDER BY comic.title;",
            &parody,
        )
    }

    pub fn get_tag_comics(&self, tag: String) -> Result<Vec<Comic>, Error> {
        Comic::get(
            self.pool.clone(),
            "SELECT comic.id, comic.title, comic.pages FROM comics comic LEFT JOIN comic_tags comic_tag ON comic.id = comic_tag.comic_id WHERE comic_tag.tag_id = ? ORDER BY comic.title;",
            &tag,
        )
    }

    pub fn add_comic(&self, form: AddForm) -> Result<Uuid, Error> {
        let conn = self.pool.get()?;

        match conn
            .query_row(
                "SELECT id FROM comics WHERE title = ? AND pages = ? LIMIT 1;",
                &[&form.title as &ToSql, &form.pages as &ToSql],
                |row| row.get::<&str, String>("id"),
            )
            .optional()?
        {
            Some(id) => {
                let uuid = Uuid::parse_str(&id)?;

                Ok(uuid)
            }
            None => {
                let uuid = Uuid::new_v4();
                let utc: DateTime<Utc> = Utc::now();

                conn.execute(
                    "INSERT INTO comics(id, title, pages, created, updated) VALUES (?, ?, ?, ?, ?);",
                    &[
                        &uuid.to_string() as &ToSql,
                        &form.title as &ToSql,
                        &form.pages as &ToSql,
                        &utc as &ToSql,
                        &utc as &ToSql,
                    ],
                )?;

                Ok(uuid)
            }
        }
    }
}
