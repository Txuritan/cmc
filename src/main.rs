mod database;
mod error;
mod helpers;
mod models;
mod pages;

use {
    crate::{
        database::Database,
        pages::{Add, Comic, Index, Page},
    },
    actix_http::KeepAlive,
    actix_web::{web, App, HttpServer},
    std::{
        io,
        net::{IpAddr, Ipv4Addr, SocketAddr},
    },
};

pub const SIZE: (u8, u8) = (145, 200);
pub const STYLE: &str = include_str!("../polygon.min.css");
pub const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() -> io::Result<()> {
    let sys = actix_rt::System::new("cmc3");

    HttpServer::new(|| {
        App::new()
            .data(Database::new())
            .route("/", web::get().to_async(Index::index))
            //
            .route("/artist/{id}", web::get().to_async(Index::artist))
            .route("/category/{id}", web::get().to_async(Index::category))
            .route("/character/{id}", web::get().to_async(Index::character))
            .route("/group/{id}", web::get().to_async(Index::group))
            .route("/parody/{id}", web::get().to_async(Index::parody))
            .route("/tag/{id}", web::get().to_async(Index::tag))
            //
            .route("/comic/{id}", web::get().to_async(Comic::get))
            .route("/comic/{id}/page/{page}", web::get().to_async(Page::get))
            //
            .route("/search", web::get().to_async(Index::search))
            //
            .service(web::resource("/add").route(web::get().to_async(Add::get)).route(web::post().to_async(Add::post)))
            //
            .service(actix_files::Files::new("/static", "store"))
    })
    .keep_alive(KeepAlive::Timeout(60))
    .bind(SocketAddr::new(
        IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
        8900,
    ))?
    .start();

    sys.run()
}
