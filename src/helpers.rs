pub trait IsEven {
    fn is_even(&self) -> bool;
}

pub trait IsOdd {
    fn is_odd(&self) -> bool;
}

macro_rules! is_impl {
    ($($t:tt)*) => {
        $(
            impl IsEven for $t {
                fn is_even(&self) -> bool {
                    self & 1 == 0
                }
            }

            impl IsOdd for $t {
                fn is_odd(&self) -> bool {
                    self & 1 != 0
                }
            }
        )*
    };
}

is_impl!(i8 u8 i16 u16 i32 u32 i64 u64 usize);
