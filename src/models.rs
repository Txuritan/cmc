use {
    crate::{database::Pool, error::Error},
    derive_more::Display,
    rusqlite::ToSql,
};

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Comic {
    pub id: String,
    pub title: String,
    pub category: Category,
    pub pages: u32,
    pub artists: Vec<Artist>,
    pub authors: Vec<Author>,
    pub characters: Vec<Character>,
    pub groups: Vec<Group>,
    pub parodies: Vec<Parody>,
    pub tags: Vec<Tag>,
}

impl Comic {
    pub fn get(pool: Pool, statement: &str, id: impl ToSql) -> Result<Vec<Self>, Error> {
        let conn = pool.get()?;
        let mut stmt = conn.prepare(statement)?;
        let comic_rows =
            stmt.query_map(&[id], |row| -> rusqlite::Result<(String, String, u32)> {
                Ok((row.get("id")?, row.get("title")?, row.get("pages")?))
            })?;

        comic_rows
            .map(|comic_row| {
                comic_row
                    .map_err(Error::from)
                    .and_then(|row| Comic::from_row(pool.clone(), row))
            })
            .collect()
    }

    pub fn from_row(pool: Pool, row: (String, String, u32)) -> Result<Self, Error> {
        let comic = Comic {
            title: row.1,
            category: Category::get(pool.clone(), &row.0)?,
            pages: row.2,
            artists: Artist::get_all_for_comic(pool.clone(), &row.0)?,
            authors: Author::get_all_for_comic(pool.clone(), &row.0)?,
            characters: Character::get_all_for_comic(pool.clone(), &row.0)?,
            groups: Group::get_all_for_comic(pool.clone(), &row.0)?,
            parodies: Parody::get_all_for_comic(pool.clone(), &row.0)?,
            tags: Tag::get_all_for_comic(pool.clone(), &row.0)?,
            id: row.0,
        };

        Ok(comic)
    }
}

#[derive(Display)]
#[display(fmt = "<a class=\"title\" href=\"/artist/{}\">{}</a>", id, name)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Artist {
    pub id: String,
    pub name: String,
}

impl Artist {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Self>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
            "SELECT artist.id, artist.name FROM comic_artists comic_artist LEFT JOIN artists artist ON comic_artist.artist_id = artist.id WHERE comic_artist.comic_id = ? ORDER BY artist.name;"
        )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                name: row.get("name")?,
            })
        })?;

        let mut artists = Vec::new();

        for artist in rows {
            artists.push(artist?);
        }

        Ok(artists)
    }
}

#[derive(Display)]
#[display(fmt = "<a class=\"title\" href=\"/author/{}\">{}</a>", id, name)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Author {
    pub id: String,
    pub name: String,
}

impl Author {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Self>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
            "SELECT author.id, author.name FROM comic_authors comic_author LEFT JOIN authors author ON comic_author.author_id = author.id WHERE comic_author.comic_id = ? ORDER BY author.name;"
        )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                name: row.get("name")?,
            })
        })?;

        let mut authors = Vec::new();

        for author in rows {
            authors.push(author?);
        }

        Ok(authors)
    }
}

#[derive(Display)]
#[display(fmt = "<a href=\"/category/{}\">{}</a>", id, name)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Category {
    pub id: String,
    pub name: String,
}

impl Category {
    pub fn get(pool: Pool, comic: &str) -> Result<Category, Error> {
        let conn = pool.get()?;

        let row = conn.query_row(
                "SELECT category.id, category.name FROM comic_category comic_category LEFT JOIN categories category ON comic_category.category_id = category.id WHERE comic_category.comic_id = ? ORDER BY category.name LIMIT 1;",
                &[&comic],
                |row| -> rusqlite::Result<Self> {
                    Ok(Self {
                        id: row.get("id")?,
                        name: row.get("name")?,
                    })
                }
            )?;

        Ok(row)
    }
}

#[derive(Display)]
#[display(fmt = "<a href=\"/character/{}\">{}</a>", id, name)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Character {
    pub id: String,
    pub name: String,
}

impl Character {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Self>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
                "SELECT c.id, c.name FROM comic_characters cc LEFT JOIN characters c ON cc.character_id = c.id WHERE cc.comic_id = ? ORDER BY c.name;"
            )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                name: row.get("name")?,
            })
        })?;

        let mut characters = Vec::new();

        for character in rows {
            characters.push(character?);
        }

        Ok(characters)
    }
}

#[derive(Display)]
#[display(fmt = "<a href=\"/group/{}\">{}</a>", id, name)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Group {
    pub id: String,
    pub name: String,
}

impl Group {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Group>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
                "SELECT g.id, g.name FROM comic_groups cg LEFT JOIN groups g ON cg.group_id = g.id WHERE cg.comic_id = ? ORDER BY g.name;"
            )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                name: row.get("name")?,
            })
        })?;

        let mut groups = Vec::new();

        for group in rows {
            groups.push(group?);
        }

        Ok(groups)
    }
}

#[derive(Display)]
#[display(fmt = "<a href=\"/parody/{}\">{}</a>", id, title)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Parody {
    pub id: String,
    pub title: String,
}

impl Parody {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Parody>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
            "SELECT p.id, p.title FROM comic_parodies cp LEFT JOIN parodies p ON cp.parody_id = p.id WHERE cp.comic_id = ? ORDER BY p.title;"
        )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                title: row.get("title")?,
            })
        })?;

        let mut parodies = Vec::new();

        for parody in rows {
            parodies.push(parody?);
        }

        Ok(parodies)
    }
}

#[derive(Display)]
#[display(fmt = "<a href=\"/tag/{}\">{}</a>", id, title)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Tag {
    pub id: String,
    pub title: String,
}

impl Tag {
    pub fn get_all_for_comic(pool: Pool, comic: &str) -> Result<Vec<Tag>, Error> {
        let conn = pool.get()?;

        let mut stmt = conn.prepare(
                "SELECT t.id, t.title FROM comic_tags ct LEFT JOIN tags t ON ct.tag_id = t.id WHERE ct.comic_id = ? ORDER BY t.title;"
            )?;

        let rows = stmt.query_map(&[&comic], |row| -> rusqlite::Result<Self> {
            Ok(Self {
                id: row.get("id")?,
                title: row.get("title")?,
            })
        })?;

        let mut tags = Vec::new();

        for tag in rows {
            tags.push(tag?);
        }

        Ok(tags)
    }
}
