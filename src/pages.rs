use {
    crate::{database::Database, error::Error, helpers::*, STYLE, VERSION},
    actix_web::{
        http,
        web::{self, Data, Form, Path, Query},
        Either, Error as AWError, HttpResponse,
    },
    askama::Template,
    chrono::{Duration, DateTime, Utc},
    futures::future::Future,
    serde::Deserialize,
    uuid::Uuid,
};

#[derive(Deserialize)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct AddForm {
    pub title: String,
    pub artists: String,
    pub authors: String,
    pub category: String,
    pub pages: u32,
    pub series: String,
    pub position: u32,
    pub parody: String,
    pub characters: String,
    pub tags: String,
}

#[derive(Template)]
#[template(path = "add.html")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Add {
    pub css: &'static str,
    pub version: &'static str,
    pub duration: Duration,
}

#[derive(Template)]
#[template(path = "add_post.html")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct AddPost {
    pub css: &'static str,
    pub version: &'static str,
    pub id: Uuid,
    pub duration: Duration,
}

impl Add {
    pub fn get(_state: Data<Database>) -> impl Future<Item = HttpResponse, Error = AWError> {
        let time = Utc::now();

        web::block(move || {
            Add {
                css: STYLE,
                version: VERSION,
                duration: Utc::now().signed_duration_since(time),
            }
            .render()
            .map_err(Error::from)
        })
        .from_err()
        .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn post(
        (state, form): (Data<Database>, Form<AddForm>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        let time = Utc::now();

        web::block(move || {
            let id = state.add_comic(form.into_inner())?;

            AddPost {
                css: STYLE,
                version: VERSION,
                id,
                duration: Utc::now().signed_duration_since(time),
            }
            .render()
            .map_err(Error::from)
        })
        .from_err()
        .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }
}

#[derive(Deserialize)]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Search {
    pub search: String,
}

#[derive(Template)]
#[template(path = "index.html")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Index {
    pub css: &'static str,
    pub version: &'static str,
    pub comics: Vec<crate::models::Comic>,
    pub search: Option<String>,
    pub duration: Duration,
}

impl Index {
    fn render(time: DateTime<Utc>, comics: Vec<crate::models::Comic>, search: Option<String>) -> Result<String, Error> {
        Index {
            css: STYLE,
            version: VERSION,
            comics,
            search,
            duration: Utc::now().signed_duration_since(time),
        }
        .render()
        .map_err(Error::from)
    }
}

impl Index {
    pub fn index(state: Data<Database>) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_all_comics()?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn artist(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_artist_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn category(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_category_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn character(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_character_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn group(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_group_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn parody(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_parody_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn tag(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Index::render(Utc::now(), state.get_tag_comics(path.into_inner())?, None))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }

    pub fn search(
        (state, form): (Data<Database>, Query<Search>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || {
            let search = form.into_inner().search;

            Index::render(Utc::now(), state.search_comics(&search)?, Some(search))
        })
        .from_err()
        .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }
}

#[derive(Template)]
#[template(path = "comic.html")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Comic {
    pub css: &'static str,
    pub version: &'static str,
    pub comic: crate::models::Comic,
    pub duration: Duration,
}

impl Comic {
    fn render(time: DateTime<Utc>, comic: crate::models::Comic) -> Result<String, Error> {
        Comic {
            css: STYLE,
            version: VERSION,
            comic,
            duration: Utc::now().signed_duration_since(time),
        }
        .render()
        .map_err(Error::from)
    }
}

impl Comic {
    pub fn get(
        (state, path): (Data<Database>, Path<String>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        web::block(move || Comic::render(Utc::now(), state.get_comic(path.into_inner())?))
            .from_err()
            .and_then(|res| Ok(HttpResponse::Ok().body(res)))
    }
}

#[derive(Template)]
#[template(path = "page.html")]
#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Page {
    pub css: &'static str,
    pub version: &'static str,
    pub comic: crate::models::Comic,
    pub page: u32,
    pub next: u32,
    pub prev: u32,
    pub duration: Duration,
}

impl Page {
    fn render(
        time: DateTime<Utc>, 
        comic: crate::models::Comic,
        page: u32,
        next: u32,
        prev: u32,
    ) -> Result<Either<String, String>, Error> {
        let body = Page {
            css: STYLE,
            version: VERSION,
            comic,
            page,
            next,
            prev,
            duration: Utc::now().signed_duration_since(time),
        }
        .render()?;

        Ok(Either::A(body))
    }
}

impl Page {
    pub fn get(
        (state, path): (Data<Database>, Path<(String, u32)>),
    ) -> impl Future<Item = HttpResponse, Error = AWError> {
        let (comic, page) = path.into_inner();
        let time = Utc::now();

        web::block(move || {
            let one = state.get_comic(comic)?;

            if page <= one.pages {
                Page::render(time, one, page, page + 1, page - 1)
            } else {
                Ok(Either::B(format!("/comic/{}", one.id)))
            }
        })
        .from_err()
        .and_then(|res| match res {
            Either::A(body) => HttpResponse::Ok().body(body),
            Either::B(header) => HttpResponse::MovedPermanently()
                .header(http::header::LOCATION, header)
                .finish(),
        })
    }
}
