use {
    actix_web::{HttpResponse, ResponseError},
    askama::Error as AskamaError,
    r2d2::Error as R2d2Error,
    rusqlite::Error as SqliteError,
    std::{borrow::Cow, error::Error as StdError, fmt},
    uuid::{parser::ParseError as UuidParserError, Error as UuidError},
};

#[derive(Debug)]
pub enum Kind {
    Askama(AskamaError),
    R2D2(R2d2Error),
    SQLite(SqliteError),
    Uuid(UuidError),
    UuidParser(UuidParserError),
    Custom(Box<StdError + Send + Sync>),
}

pub struct Error {
    pub kind: Kind,
    pub details: Cow<'static, str>,
}

impl Error {
    pub fn new<I>(kind: Kind, details: I) -> Error
    where
        I: Into<Cow<'static, str>>,
    {
        Error {
            kind,
            details: details.into(),
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.details.len() > 0 {
            write!(f, "Error <{:?}>: {}", self.kind, self.details)
        } else {
            write!(f, "Error <{:?}>", self.kind)
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.details.len() > 0 {
            write!(f, "{}: {}", self.description(), self.details)
        } else {
            write!(f, "{}", self.description())
        }
    }
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::InternalServerError().finish()
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match self.kind {
            Kind::Askama(ref err) => err.description(),
            Kind::R2D2(ref err) => err.description(),
            Kind::SQLite(ref err) => err.description(),
            Kind::Uuid(ref err) => err.description(),
            Kind::UuidParser(ref err) => err.description(),
            Kind::Custom(ref err) => err.description(),
        }
    }
}

impl From<AskamaError> for Error {
    fn from(err: AskamaError) -> Error {
        Error::new(Kind::Askama(err), "")
    }
}

impl From<R2d2Error> for Error {
    fn from(err: R2d2Error) -> Error {
        Error::new(Kind::R2D2(err), "")
    }
}

impl From<SqliteError> for Error {
    fn from(err: SqliteError) -> Error {
        Error::new(Kind::SQLite(err), "")
    }
}

impl From<UuidError> for Error {
    fn from(err: UuidError) -> Error {
        Error::new(Kind::Uuid(err), "")
    }
}

impl From<UuidParserError> for Error {
    fn from(err: UuidParserError) -> Error {
        Error::new(Kind::UuidParser(err), "")
    }
}

impl<B> From<Box<B>> for Error
where
    B: StdError + Send + Sync + 'static,
{
    fn from(err: Box<B>) -> Error {
        Error::new(Kind::Custom(err), "")
    }
}
